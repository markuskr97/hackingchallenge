from flask import Flask
from flask import render_template
from flask import request
from flask import make_response
from flask import redirect
from flask import url_for
from pexpect import pxssh
import time
import _thread
import random
import json
import requests


app = Flask(__name__)
 
cookieName = "FlaskSession"

#'sessionKey':'teamName'
sessionList = {}

#challenge = {'name', 'overview', 'description', 'link', 'flag', 'points'}
challenges = []

# teamName:{'score', 'motive', 'password', 'solved'}
teams = {}

#flag = {'flag', 'challenge', 'points'}
flags = []

WebGoatAddress = "192.168.178.43"
HackPiAddress = "192.168.178.41"

@app.route("/")
def index():
    if checkStatus(request) == False:
        return render_template('tempLogin.html')
    
    scoreTeams = []
    scorePoints = []

    for team in teams:
        scoreTeams.append(team)
        scorePoints.append(teams[team]['score'])
    return render_template('tempIndex.html', teamList=scoreTeams, pointList = scorePoints)

@app.route("/challenges")
def viewChallenges():
    if checkStatus(request) == False:
        return render_template('tempLogin.html')
    userId = request.cookies.get(cookieName)
    solvChallenges = teams[sessionList[userId]]['solved']
    valChallenges = []
    for item in challenges:
        if item['name'] not in solvChallenges:
            valChallenges.append(item)
    return render_template('tempChallenges.html', challenges=valChallenges)

@app.route("/challenges/<challenge>")
def viewSingleChallenge(challenge):
    if checkStatus(request) == False:
        return render_template('tempLogin.html')
    
    for item in challenges:
        if item['link'] == ("challenges/" + challenge):
            return render_template('tempSingleChallenge.html', challenge=item)

    return render_template('tempChallenges.html', challenges=challenges)


@app.route("/login", methods=['GET','POST'])
def logIn():
    if checkStatus(request):
        return render_template('tempIndex.html')
    if request.method == "POST":
        userName = request.form['name']
        password = request.form['password']
        try:
            team = teams[userName]
        except:
            message = "wrong username or password"
            return render_template('tempLogin.html', message = message)
        if (team != None and password == team['password']):
            randKey = str(random.getrandbits(128))
            sessionList[randKey] = userName
            resp = make_response(redirect('/'))
            resp.set_cookie(cookieName, randKey)
            return resp
        else:
            message = "wrong username or password"
            return render_template('tempLogin.html', message = message)
    elif request.method == "GET":
        return render_template('tempLogin.html')

@app.route("/register", methods=['GET','POST'])
def register():
    if checkStatus(request):
        return render_template('tempIndex.html')
    if request.method == "POST":
        teamName = request.form['name']
        motive = request.form['motive']
        password = request.form['password']

        if len(teamName) < 6 or len(password) < 6:
            return render_template('tempRegister.html', message="password or username too short.")

        try: 
            if (teams[teamName] != None):
                message = "Team already exists"
                return render_template('tempLogin.html', message = message)
        except:
            message = "Team created successfully"
            try:
                print (createNewWebGoatUser(teamName, password))
                print (initSSH(teamName, password))
            except:
                message = "Cold not create users on webgoat and on pi ..."
                return render_template('tempRegister.html', message=message)
            teams[teamName] = {'motive':motive,'score':0, 'password':password, 'solved':[]}
            return render_template('tempLogin.html', message = message)
    elif request.method == "GET":
        return render_template('tempRegister.html')


@app.route("/validate", methods=['post'])
def validateFlag():
    if checkStatus(request) == False:
        return render_template('tempLogin.html')
    flag = request.form['flag']
    userId = request.cookies.get(cookieName)
    for item in flags:
        if item['flag'] == flag and (item['challenge'] not in teams[sessionList[userId]]['solved']):
            teams[sessionList[userId]]['solved'].append(item['challenge'])
            teams[sessionList[userId]]['score'] += item['points']
            return redirect('challenges')
    return redirect('challenges')

@app.route("/logout", methods=['get'])
def logout():
    if checkStatus(request):
        userId = request.cookies.get(cookieName)
        try:
            del sessionList[userId]
            message = "Logged out successfully"
            return render_template('tempLogin.html', message = message)
        except:
            message = "already logged out"
            return render_template('tempLogin.html', message = message)
    else:
        return render_template('tempLogin.html')

@app.route("/about", methods=['get'])
def about():
    if checkStatus(request) == False:
        return render_template('tempLogin.html')
    else:
        return render_template('tempAbout.html')
    

def checkStatus(request):
    userId = request.cookies.get(cookieName)
    if userId != None:
        try:
            if (sessionList[userId] != None):
                return True
        except:
            pass
        return False
    else:   
        return False

def initSSH(userName, password):
    session = pxssh.pxssh()
    
    if not session.login (HackPiAddress, 'pi', 'RedCube18'):
        print ("SSH session failed on login.")
        print (str(session))
        return False
    
    else:
        print ("SSH session login successful")
        session.sendline ('sudo /home/pi/Data/createUser.sh '+userName + ' ' + password)
        success = session.prompt()
        
        if not success:
            return False

        print (session.before)
        session.logout()
        return True
    

def initFlags():
    for item in challenges:
        global flags
        flags.append({'challenge' : item['name'], 'flag' : item['flag'], 'points' : item['points']})
    print (flags)


def createNewWebGoatUser(username, password):
    payload = "agree=agree"+"&matchingPassword=" + password + "&password="+ password + "&username=" + username
    r = requests.post("http://" + WebGoatAddress + ":8181/WebGoat/register.mvc", data=payload)
    print(r.request.body)
    print(r.request.headers)
    print(r)
    if r.status_code == 200 or r.status_code == 302:
        return True
    else:
        return False

def writeTeams():
    with open('teams.json', 'w') as outfile:  
        json.dump(teams, outfile)

def readTeams():
    with open('teams.json') as json_file:
        global teams  
        teams = json.load(json_file)

def readChallenges():
    with open('challenges.json') as json_file:  
        data = json.load(json_file)
    global challenges
    challenges = data['challenges']

def storeData():
    while True:
        try:
            writeTeams()
            print ("Stored teams ...")
        except:
            print('Could not store data ...')
        time.sleep(60)

 
if __name__ == "__main__":
    try:
        readChallenges()
    except:
        print ('Could not read challenges ...')
    try:
        readTeams()
    except:
        print('No teams found ... ')
    _thread.start_new_thread(storeData,() )
    initFlags()
    app.run(host="0.0.0.0")
