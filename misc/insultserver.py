#! /usr/bin/python2
import SocketServer
import random

class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """
    insults=["Is your ass jealous of the amount of shit that just came out of your mouth?",
	 "I'm not saying I hate you, but I would unplug your life support to charge my phone.",
	 "Roses are red, violets are blue, I have 5 fingers, the 3rd ones for you.",
	 "Your birth certificate is an apology letter from the condom factory.",
	 "If you are going to be two faced, at least make one of them pretty.",
	 "You must have been born on a highway because that's where most accidents happen.",
	 "I wasn't born with enough middle fingers to let you know how I feel about you.",
	 "Yo're so ugly, when your mom dropped you off at school she got a fine for littering.",
	 "I bet your brain feels as good as new, seeing that you never use it.",
	 "What's the difference between you and eggs? Eggs get laid and you don't.",
	 "There's only one problem with your face, I can see it.",
	 "If you're gonna be a smartass, first you have to be smart. Otherwise you're just an ass.",
	 "How many times do I have to flush to get rid of you?",
	 "Shut up, you'll never be the man your mother is." ]
    def handle(self):
        self.request.sendall(self.insults[random.randint(0, len(self.insults)-1)] + "\n")
        # self.request is the TCP socket connected to the client

def main():
    HOST, PORT = "0.0.0.0", 3319

    # Create the server, binding to localhost on port 4242
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()

if __name__ == '__main__':
    main()
